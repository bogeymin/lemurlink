[Migrated to GitHub](https://github.com/bogeymin/lemurlink)

# Lemur Link

A link management tool based on the Wagtail CMS.

To install:

	pip install https://bitbucket.org/bogeymin/lemurlink/get/master.zip;

Or in your requirements file:

	-e git+https://bitbucket.org/bogeymin/lemurlink/get/master.zip

Or in your ``setup.py`` file:

	install_requires=["lemurlink"],
	dependency_links=[
		"https://bitbucket.org/bogeymin/lemurlink/get/master.zip#egg=lemurlink",
	]