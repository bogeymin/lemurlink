# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, PageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtailmodelchooser.edit_handlers import register_chooser_for_model
from utils import get_url_title

# Exports

__all__ = (
    "CarouselItemModel",
    "LinkFieldsModel",
    "RelatedLinkModel",
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Abstract


class LinkFieldsModel(models.Model):
    """Abstract model for creating a connection to a URL, document, or page
    within the CMS.
    """
    link_external = models.URLField(
        _("External Link"),
        blank=True,
        help_text=_("Enter the full URL."),
        max_length=1024,
        null=True,
    )

    link_page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        help_text=_("Link to another page on the site."),
        related_name='+',
        verbose_name=_("Link to Page")
    )

    link_document = models.ForeignKey(
        "wagtaildocs.Document",
        null=True,
        blank=True,
        help_text=_("Attach a document."),
        related_name='+',
        verbose_name=_("Link to Document")
    )

    def get_link(self):
        """Get the actual URL from the local page, document or external link
        (in that order).


        :rtype: str

        """
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    def get_title(self):
        """Get the title of the link from the external URL, page, or document (in that order).

        :rtype: str

        .. note::
            If no title is available, an empty string is returned.

        """
        if self.link_external:
            # noinspection PyTypeChecker
            return get_url_title(self.link_external)
        elif self.link_page:
            return self.link_page.title
        elif self.link_document:
            return self.link_document.title
        else:
            return ""

    @property
    def link(self):
        return self.get_link()

    @property
    def title(self):
        return self.get_title()

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    class Meta:
        abstract = True


class CarouselItemModel(LinkFieldsModel):
    """A carousel item is an image that appears in a slider or slideshow."""

    caption = models.CharField(
        _("caption"),
        blank=True,
        help_text=_("An optional caption for the image."),
        max_length=256,
        null=True
    )

    image = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        help_text=_("Select the image to be displayed."),
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("image")
    )

    panels = [
        ImageChooserPanel("image"),
        FieldPanel("caption"),
        MultiFieldPanel(LinkFieldsModel.panels, "Link"),
    ]

    class Meta:
        abstract = True

    def get_title(self):
        """Get the title from the image or caption (in that order) or from the parent class, if possible."""
        if self.image:
            return self.image.title
        elif self.caption:
            return self.caption
        else:
            return super(CarouselItemModel, self).get_title()


class RelatedLinkModel(LinkFieldsModel):
    """Allows related URLs, pages, or documents to be connected to a page."""

    is_embedded = models.BooleanField(
        _("embed"),
        default=False,
        help_text=_("Indicates the selected link should be embedded on the "
                    "page that utilizes the related link. When the link "
                    "is external or a document, auto-embedding is attempted. "
                    "When the link is another page, it is displayed as link.")
    )

    label = models.CharField(
        _("title"),
        blank=True,
        help_text=_("Specify a link title. If omitted, the title of the selected resource will be used."),
        max_length=256,
        null=True
    )

    panels = [
        FieldPanel('label'),
        MultiFieldPanel(LinkFieldsModel.panels, _("Link")),
        FieldPanel("is_embedded"),
    ]

    class Meta:
        abstract = True

    def get_title(self):
        """Get the title from the current instance or from the parent class, if possible."""
        if self.label:
            return self.label
        else:
            return super(RelatedLinkModel, self).get_title()

# Models

'''
class LinkIndexPage(Page):
    """An index of classifications."""

    body = RichTextField(
        blank=True,
        help_text=_("The body of the page appears before any entry listing. "),
        null=True
    )

    intro = models.TextField(
        _("intro"),
        blank=True,
        help_text=_("The introduction appears in lists."),
        null=True
    )

    content_panels = [
        FieldPanel("title", classname="full title"),
        FieldPanel("intro", classname="full"),
        FieldPanel("body", classname="full"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("body"),
        index.SearchField("intro"),
    ]

    class Meta:
        verbose_name = _("Taxonomy Index Page")
        verbose_name_plural = _("Taxonomy Index Pages")

    def get_context(self, request, *args, **kwargs):
        # Get the objects.
        objects = self.get_subpages()

        # Pagination
        page = request.GET.get('page')
        paginator = Paginator(objects, ENTRIES_PER_PAGE)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        # Update template context
        context = super(LinkIndexPage, self).get_context(
            request,
            *args,
            **kwargs
        )
        context['pages'] = objects
        return context

    def get_subpages(self):
        """Get the pages associated with the page.

        :rtype: QuerySet

        """
        return LinkPage.objects.live().descendant_of(self)

    @property
    def subpages(self):
        return self.get_subpages()


class LinkPage(Page):
    """A taxonomy page is a collection of specific classifications.

    .. tip::
        Most of the time, the page title will match the category label.

    """

    body = RichTextField(
        blank=True,
        help_text=_("The body of the page appears before any entry listing. "),
        null=True
    )

    category = models.ForeignKey(
        Category,
        help_text=_("The category of content to be displayed on this page."),
        on_delete=models.PROTECT,
        related_name="taxonomy_pages",
        verbose_name=_("category")
    )

    intro = models.TextField(
        _("intro"),
        blank=True,
        help_text=_("The introduction appears in lists."),
        null=True
    )

    image = models.ForeignKey(
        "wagtailimages.Image",
        blank=True,
        help_text=_("Select an image to use for the page."),
        null=True,
        on_delete=models.SET_NULL,
        related_name="+",
        verbose_name=_("image")
    )

    content_panels = [
        FieldPanel("title", classname="full title"),
        CategoryChooserPanel("category"),
        FieldPanel("intro", classname="full"),
        FieldPanel("body", classname="full"),
    ]

    parent_page_types = (
        LinkIndexPage,
    )

    promote_panels = Page.promote_panels + [
        ImageChooserPanel("image"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
    ]

    class Meta:
        ordering = ["title"]
        verbose_name = _("Taxonomy Page")
        verbose_name_plural = _("Taxonomy Pages")

    def get_entries(self):
        """Get the entries that have been associated with the page.

        :rtype: list

        """
        a = list()

        qs = self.get_children()
        for row in qs:

            # Get the specific (lowest-level) child instance.
            page = row.specific

            # The page may not have implemented a category.
            match = False
            try:
                if page.category == self.category:
                    match = True
            except AttributeError:
                pass

            # Save the page if we have a matching category.
            if match:
                a.append(page)

        return a

    @property
    def total_entries(self):
        # TODO: Getting the entries and counting them is really inefficient.
        # Could we cache the result?
        return len(self.get_entries())

# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models
from django.forms.utils import flatatt
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from taggit.managers import TaggableManager

# Exports

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

VERIFY_HOURLY = "hourly"
VERIFY_DAILY = "daily"
VERIFY_WEEKLY = "weekly"
VERIFY_MONTHLY = "monthly"
VERIFY_QUARTERLY = "quarterly"
VERIFY_ANNUALLY = "annually"
VERIFY_NEVER = "never"
VERIFICATION_FREQUENCIES = (
    (VERIFY_HOURLY, _("Every Hour")),
    (VERIFY_DAILY, _("Every Day")),
    (VERIFY_WEEKLY, _("Every Week")),
    (VERIFY_MONTHLY, _("Every Month")),
    (VERIFY_QUARTERLY, _("Every Quarter")),
    (VERIFY_ANNUALLY, _("Once Per Year")),
    (VERIFY_NEVER, _("No Verification")),
)

# Models


class Category(models.Model):
    """A link category."""

    cached_total_hits = models.IntegerField(
        _("total hits"),
        default=0,
        help_text=_("Total number of times the links in this category have"
                    " been viewed.")
    )

    cached_total_links = models.IntegerField(
        _("total links"),
        default=0,
        help_text=_("Total number of links in this category")
    )

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("Optional description of the group."),
        null=True
    )

    icon = models.CharField(
        _("icon"),
        blank=True,
        help_text=_("The icon to display for the category. Choose from: "
                    "http://fontawesome.io/icons/"),
        max_length=64,
        null=True
    )

    is_enabled = models.NullBooleanField(
        _("enabled"),
        help_text=_("Indicates whether the group should be available to users.")
    )

    label = models.CharField(
        _("label"),
        help_text=_("The official name or title of the category."),
        max_length=128,
        unique=True
    )

    thumbnail = models.FileField(
        _("thumbnail"),
        blank=True,
        help_text=_("Select a thumbnail image to display for the category."),
        max_length=1024,
        null=True
    )

    verification_frequency = models.CharField(
        _("verification frequency"),
        choices=VERIFICATION_FREQUENCIES,
        default=VERIFY_NEVER,
        help_text=_("How often should the links in this category be checked?"),
        max_length=32
    )

    class Meta:
        ordering = ["label"]
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __unicode__(self):
        return u"%s" % self.label

    def get_total_hits(self):
        """Get the total number of times the links in this category have
        been clicked.

        :rtype: int

        """
        count = list()
        for k in self.links.all():
            count.append(k.get_total_hits())

        return sum(count)

    def get_total_links(self):
        """Get the total number of links in the group.

        :rtype: int

        """
        return self.links.all().count()

    def save(self, *args, **kwargs):
        self.cached_total_hits = self.get_total_hits()
        self.cached_total_links = self.get_total_links()
        super(Category, self).save(*args, **kwargs)

    @property
    def total_hits(self):
        return self.get_total_hits()

    @property
    def total_links(self):
        return self.get_total_links()


class Type(models.Model):
    """Represents a type or class of link."""

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("Optional description of the group."),
        null=True
    )

    label = models.CharField(
        _("label"),
        help_text=_("The official name or title of the category."),
        max_length=128,
        unique=True
    )

    class Meta:
        verbose_name = _("Type")
        verbose_name_plural = _("Types")

    def __unicode__(self):
        return u"%s" % self.label


class Link(models.Model):
    """A link to an internal or external resource or location."""

    cached_total_hits = models.IntegerField(
        _("total hits"),
        default=0,
        help_text=_("Total number of times the link has been viewed.")
    )

    category = models.ForeignKey(
        Category,
        help_text=_("Select a category."),
        related_name="links",
        verbose_name=_("category")
    )

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("Optional description of the link."),
        null=True
    )

    download_name = models.CharField(
        _("download name"),
        blank=True,
        help_text=_("Specify the pre-filled file name of the download."),
        max_length=128,
        null=True
    )

    groups = models.ManyToManyField(
        Group,
        blank=True,
        help_text=_("User groups, if any, that will be most interested in "
                    "this link. Leave blank for display to all groups."),
        related_name="links",
        verbose_name=_("groups")
    )

    href = models.CharField(
        _("url"),
        help_text=_("Enter the URL (partial or fully-qualified)."),
        max_length=1024
    )

    icon = models.CharField(
        _("icon"),
        blank=True,
        help_text=_("The icon to display for the link. Choose from: "
                    "http://fontawesome.io/icons/"),
        max_length=64,
        null=True
    )

    is_download = models.BooleanField(
        _("downloadable"),
        default=False,
        help_text=_("Indicates the link is a download.")
    )

    is_enabled = models.BooleanField(
        _("enabled"),
        default=True,
        help_text=_("Indicates whether the link should be available to users.")
    )

    # TODO: Create an algorithm that combines Hit, Rating, and Favorite to
    # determine whether Resource.is_popular should be True. This could be
    # incorporated into a management command and cron job. For now, we will
    # simply mark is_popular manually.
    is_popular = models.BooleanField(
        _("popular"),
        default=False,
        help_text=_("Indicates the resource is popular.")
    )

    sort_order = models.PositiveSmallIntegerField(
        _("sort order"),
        blank=True,
        help_text=_("Order in which the link should appear in lists."),
        null=True
    )

    tags = TaggableManager()

    TARGET_SELF = "_self"
    TARGET_BLANK = "_blank"
    TARGET_PARENT = "_parent"
    TARGET_TOP = "_top"
    TARGETS = (
        (TARGET_SELF, _("Same Window")),
        (TARGET_BLANK, _("New Window")),
        (TARGET_PARENT, _("Parent")),
        (TARGET_TOP, _("Top")),
    )
    target = models.CharField(
        _("target"),
        choices=TARGETS,
        default=TARGET_SELF,
        max_length=32
    )

    text = models.CharField(
        _("text"),
        help_text=_("The text displayed as the link."),
        max_length=128
    )

    thumbnail = models.FileField(
        _("thumbnail"),
        blank=True,
        help_text=_("Select a thumbnail image to display for the link."),
        max_length=1024,
        null=True
    )

    type = models.ForeignKey(
        Type,
        help_text=_("Select the type."),
        related_name="links",
        verbose_name=_("type")
    )

    title = models.CharField(
        _("title"),
        blank=True,
        help_text=_("Used for the title attribute."),
        max_length=128,
        null=True
    )

    class Meta:
        ordering = ["sort_order", "text"]
        verbose_name = _("Link")
        verbose_name_plural = _("Links")

    def __unicode__(self):
        return u"%s" % self.text

    def to_html(self):
        """Get the HTML for the link.

        :rtype: str

        """

        context = {
            'attributes': flatatt(self.get_attributes()),
            'text': self.text,
        }

        a = list()
        a.append('<a %(attributes)s>')

        if self.icon:
            context['icon'] = self.icon
            a.append('<i class="fa %(icon)s" aria-hidden="true"></i>')
            a.append('&nbsp;')

        a.append('%(text)s</a>')

        html = "".join(a) % context

        return mark_safe(html)

    @property
    def follow(self):
        """An alias for ``to_html()`` that may be used in the admin."""
        return self.to_html()

    def get_attributes(self):
        """Get the link attributes.

        :rtype: dict

        """
        d = {
            'href': self.url
        }

        if self.is_download:
            d['download'] = self.download_name or ""

        if self.title is not None:
            d['title'] = self.title

        return d

    def get_short_url(self):
        """Get the short URL."""
        base = getattr(settings, "SHORT_URL", "http://127.0.0.1/k/")
        return "%s/%s" % (base, self.hash)

    @property
    def short_url(self):
        return self.get_short_url()

    def get_total_hits(self):
        """Get the total number of times the link has been clicked.

        :rtype: int

        """
        return self.hits.all().count()

    def save(self, *args, **kwargs):
        self.cached_total_hits = self.get_total_hits()
        super(Link, self).save(*args, **kwargs)


class Verification(models.Model):
    """A verification that a link is working and the options that drive quality
    control.
    """

    enabled = models.BooleanField(
        _("enabled"),
        default=True,
        help_text=_("Enable verification for this link. Overrides category"
                    "settings.")
    )

    frequency = models.CharField(
        _("check frequency"),
        choices=VERIFICATION_FREQUENCIES,
        default=VERIFY_QUARTERLY,
        help_text=_("How often should the link be checked?"),
        max_length=32
    )

    is_broken = models.NullBooleanField(
        _("broken"),
        help_text=_("Indicates the link was broken the last time "
                    "it was checked.")
    )

    last_verified_dt = models.DateTimeField(
        _("last verified date/time"),
        blank=True,
        help_text=_("Last date and time the link was checked."),
        null=True
    )

    link = models.OneToOneField(
        Link,
        related_name="verification",
        verbose_name=_("link")
    )

    response_code = models.PositiveSmallIntegerField(
        _("response code"),
        blank=True,
        help_text=_("The HTTP response code received."),
        null=True
    )

    class Meta:
        verbose_name = _("Verification")
        verbose_name_plural = _("Verifications")

    def __unicode__(self):
        return u"%s" % self.link.text


class Hit(models.Model):
    """Hit counter for link activity."""

    # added_by_user must be blank and null since we can't says for sure that a
    # person clicking the link will be logged in.
    added_by_user = models.ForeignKey(
        AUTH_USER_MODEL,
        blank=True,
        help_text=_("The user that clicked the link."),
        null=True,
        related_name="clicked_links",
        verbose_name=_("added by")
    )

    added_dt = models.DateTimeField(
        _("added date/time"),
        auto_now_add=True,
        help_text=_("Date and time the hit was recorded.")
    )

    link = models.ForeignKey(
        Link,
        help_text=_("The link for which the hit was recorded."),
        related_name="hits",
        verbose_name=_("link")
    )

    http_accept_language = models.CharField(
        _("http accept language"),
        blank=True,
        help_text=_("Language, if any, that was requested by the browser."),
        max_length=256,
        null=True
    )

    http_referrer = models.CharField(
        _("http referrer"),
        blank=True,
        help_text=_("The page, if any, from which the link was clicked."),
        max_length=1024,
        null=True
    )

    http_user_agent = models.TextField(
        _("http user agent"),
        blank=True,
        help_text=_("The browser and platform information from the user."),
        null=True
    )

    # We don't really care about validating the IP address, so a regular
    # CharField is used. The max length of a ipv6 address is 46 characters.
    # http://stackoverflow.com/q/166132/241720
    ip_address = models.CharField(
        _("ip address"),
        blank=True,
        help_text=_("The remote address of the visitor."),
        max_length=46,
        null=True
    )

    server_name = models.CharField(
        _("server name"),
        blank=True,
        help_text=_("Useful if the application runs on multiple servers, "
                    "as with load balancing."),
        max_length=256,
        null=True
    )

    class Meta:
        get_latest_by = "added_dt"
        ordering = ["added_dt"]
        verbose_name = _("Click")
        verbose_name_plural = _("Clicks")

    def __unicode__(self):
        return str(self.added_dt)


class Favorite(models.Model):
    """A user's favorite links."""

    added_by_user = models.ForeignKey(
        AUTH_USER_MODEL,
        help_text=_("User that added the favorite."),
        related_name="favorite_links",
        verbose_name=_("added by")
    )

    added_dt = models.DateTimeField(
        _("added date/time"),
        auto_now_add=True,
        help_text=_("Date and time the favorite was added.")
    )

    link = models.ForeignKey(
        Link,
        help_text=_("Select the link."),
        related_name="favorites",
        verbose_name=_("link")
    )

    class Meta:
        ordering = ["added_dt"]
        unique_together = ("link", "added_by_user")
        verbose_name = _("Favorite")
        verbose_name_plural = _("Favorites")

    def __unicode__(self):
        return u"%s" % self.link.text


class Rating(models.Model):
    """Resource ratings by users."""

    # TODO: A comment could be added to the user's rating.

    added_by_user = models.ForeignKey(
        AUTH_USER_MODEL,
        help_text=_("User that rated the link."),
        related_name="link_ratings",
        verbose_name=_("added by")
    )

    added_dt = models.DateTimeField(
        _("added date/time"),
        auto_now_add=True,
        help_text=_("Date and time the rating was added.")
    )

    BAD_RATING = 1
    POOR_RATING = 2
    OK_RATING = 3
    GOOD_RATING = 4
    BEST_RATING = 5
    POINTS_CHOICES = (
        (BAD_RATING, _("Bad")),
        (POOR_RATING, _("Poor")),
        (OK_RATING, _("Okay")),
        (GOOD_RATING, _("Great")),
        (BEST_RATING, _("Excellent")),
    )

    points = models.PositiveSmallIntegerField(
        _("points"),
        choices=POINTS_CHOICES,
        default=OK_RATING,
        help_text=_("Points/score for the rating.")
    )

    link = models.ForeignKey(
        Link,
        help_text=_("Link with which the rating is associated."),
        related_name="ratings",
        verbose_name=_("link")
    )

    class Meta:
        ordering = ["added_dt"]
        unique_together = ("link", "added_by_user")
        verbose_name = _("Rating")
        verbose_name_plural = _("Ratings")

    def __unicode__(self):
        return u"%s" % self.link.text
'''
