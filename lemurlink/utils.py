# Imports

from bs4 import BeautifulSoup
import urllib2

# Functions


def get_url_title(url):
    """Get the value of a URL's title tag.

    :param url: The URL.
    :type url: str

    :rtype: str

    """
    soup = BeautifulSoup(urllib2.urlopen(url), "html5lib")
    return soup.title.string
