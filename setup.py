# See https://packaging.python.org/en/latest/distributing.html
# and https://docs.python.org/2/distutils/setupscript.html
# and https://pypi.python.org/pypi?%3Aaction=list_classifiers

from os.path import join, exists as path_exists
from setuptools import setup, find_packages


def read_file(path):
    with open(path, "rb") as f:
        contents = f.read()
        f.close()
    return contents


def get_description():

    files = ("README", "COPYING", "CHANGES", "TODO")
    extensions = ("markdown", "md", "rst", "txt")

    description = ""
    for file_name in files:
        for ext in extensions:
            path = "%s.%s" % (file_name, ext)
            if path_exists(path):
                description += read_file(path)

    return description


def get_version():
    return read_file("VERSION.txt")


setup(
    name='lemurlink',
    version=get_version(),
    description='A link management tool based on the Wagtail CMS.',
    long_description=get_description(),
    author='Shawn Davis',
    author_email='shawn@ptltd.co',
    url='https://www.bitbucket.org/bogeymin/lemurlink',
    packages=find_packages(),
    install_requires=["wagtail", "wagtailmodelchooser", "taxonomytiger"],
    dependency_links=[
        "https://bitbucket.org/bogeymin/taxonomytiger/get/master.zip#egg=taxonomytiger",
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Framework :: Django',
    ],
    zip_safe=False,
    tests_require=["wagtail", "wagtailmodelchooser"],
    test_suite='runtests.runtests'
)
